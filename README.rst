Projet Doc
**********

Ce projet a pour objectif de noous faire découvrir le monde de la documentation et de sa génératoin automatique avec les CIs

Lien de la documentation en ligne
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

https://anthonyleclercq.gitlab.io/projectdoc/

Pour commencer
##############

Ouvrer un terminal ou le git bash (pour windows) et placer vous de le dossier où vous souhaitez avoir le projet
tapez: git clone https://gitlab.com/anthonyleclercq/projectdoc.git

Pré-requis
##########

Les prérequis sont:
- python
- pip
- sphinx
- markdown

Installation
############

installer python via l'exécutable ou faites extraire le poackage
tapez:
 - pip install -U sphinx
 - pip install -U recommonmark

Démarrage
#########

déplacer vous dans le dossier docs et tapez la commande suivante:
 - make html
 - cd _build/html
 - ouvrez l'index.html

Fabriqué avec
#############

* [git](https://git-scm.com/) - gestionnqire de version
* [VsCode](https://code.visualstudio.com/) - Editeur de textes

Versions
########

**Dernière version stable :** 0.5
**Dernière version :** 0.6
Liste des versions : [Cliquer pour afficher](https://gitlab.com/anthonyleclercq/projectdoc/tags)

Auteurs
#######

* **Anthony Leclercq** _alias_ [@anthonyleclercq](https://gitlab.com/anthonyleclercq)
* **Ferial Oukoukas** _alias_ [@ferial_oukoukas]https://gitlab.com/ferial_oukoukas)
* **Adeniss Karti** _alias_ [@segfault00](https://gitlab.com/segfault00)
* **Sabrine Benabdallah** _alias_ [@sabrine.ben](https://gitlab.com/sabrine.ben)