.. projetDoc documentation master file, created by
   sphinx-quickstart on Wed Mar 10 09:59:34 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to projetDoc's documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   guides
   tutoriels
   faq
   referentiel




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
