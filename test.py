"""
 print hello world
"""

# This program prints Hello, world!
def hello(d):
    """ 
    fonction affecte hello
    
    Args: 
        d (str): variable du hello
    """
    d= "hello horld"
    return d

 # This program prints Hello, world!
def noHello(d):
    """ 
    fonction n'affecte pas hello
    
    :param d: variable vide
    :type d: str

    """
    d= " "
    return d